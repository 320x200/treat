\ ForthTV
\ distorter.f
\ a fullscreen vertical distorter
\ a tribute to the 90's intro distorters :)
\ aym3ric -at- goto10 -dot- org


\ variables
: init-variables
	variable counter
	1 counter !
	variable counter-saved
	1 counter-saved !
;

init-variables

\ utils
: ndup   for dup next ;
: n+	 for + next ;
: transx for nop next ;

\ counter stuff
: count   		counter @ 1 + counter ! ;
: store-counter   	counter @ counter-saved ! ;
: restore-counter	counter-saved @ counter ! ;
: counts		for count next ;

\ update line size (linear bouncexor!)
: get-size 
	counter @ 
	1st 
	4 high? if
		#x1F xor #x1F and 5 +
	else
		#x1F and 5 +
	then
	;

\ pattern and chess
: X     WHITE 1 transx ;
: _     BLACK 1 transx ;    

: X0	for X _ next ;
: 0X	for _ X next ;
	
: line1	for hsync 15 0X next ;
: line2 for hsync 15 X0 next ;

: lines
	3 counts
    	get-size dup
    	line2
    
    	3 counts
    	get-size dup
    	line1
	;

: chess	8 for lines next ;

\ finally...
: distorter

    screen ->

    hsync 
	restore-counter count store-counter
    get-size dup
    line1

    chess 
    
    16 n+ 
    249 nop swap - line2
    ;
