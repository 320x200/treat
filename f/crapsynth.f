\ CATkit
\ my crap synth

\ utils

: on   engine-on ;
: off  engine-off ;

variable ratio

: pause
	ratio !
	for
		ratio @ for next	
	next
	;



\ sounds


: snare
	noise
	on
	255 80 pause
	off
	;



: varisnare
	noise
	on
	255 4 ad@ pause
	off
	;

: powerkick1
	square
	on
	2
		100 for
		100 3 ad@ pause
		dup p0 1 +
		next
	off
	drop
	;

: powerkick2
	square
	on
	10
		100 for
		100 3 ad@ pause
		dup p0 1 +
		next
	off
	drop
	;


: powerbass
	xmod2 p0
        on
        2 ad@ p1
	100 4 ad@ pause
        off
	;

: thunderdome1
	for
		powerkick1
		100 powerbass
		50 powerbass
		100 powerbass
		powerkick1
		100 powerbass
		50 powerbass
		100 powerbass
	next
	;

: thunderdome2
	for
                powerkick2
                100 3 ad@ pause
		1 powerbass
		100 3 ad@ pause
		powerkick2
        	powerkick2
		powerkick2
		powerkick2
	next
	;

: thunderdome3
        for
                powerkick2
        	10 powerbass
		10 powerbass
		10 powerbass
		10 powerbass
		10 powerbass
		20 powerbass
		20 powerbass
	next    
        ;

: megathunderdome
	for
		3 thunderdome1
		1 thunderdome2
		2 thunderdome1
		2 thunderdome3
		1 thunderdome2
                4 thunderdome3
	next
	;


