\ CATkit
\ powerballad with your friends

\ variables
\ ---------

variable ratio
128 ratio !

\ utils
\ -----


: pause		dup ratio ! for ratio @ for next next ;
: complete 	255 swap - ;



\ sounds
\ ------

: bass
	xmod2 5 p0 10 p1
	4 ad@ pause
	;

: snare
	noise	
	4 ad@ pause
	;

\: silence
\	off
\	4 ad@ complete pause
\	on
\	;

: powerkick
	square
	1
		150 for
		10 pause
		dup p0 1 +
		3 ad@ p1
		next
	drop
	silence
	;


\ patterns
\ --------

: seq	for powerkick silence next ;

: seq2	for 
		powerkick
		wait-note
	next ;	
	


