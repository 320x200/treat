\ drb0x.f
\ 1.0
\ CLOSER TO THE METAAAAAAAAAAAaaaalll...
\ A simple drumbox with hardcoded patterns
\ and random sound picking
\ ---
\ knob0 = general tempo
\ knob1 = pattern selection for bassdrum
\ knob2 = pattern selection for hi-hat
\ knob3 = pattern selection for snares
\ knob4 = pattern selection for ride/bells
\ ---
\ button S1 = 16 steps pattern (full length)
\ button S2 = clip to the 1st 8 steps
\ button S3 = clip to the 1st 4 steps
\ button S4 = mute

\ SYSTEM
load sys.f

\ SOUNDS 
load DK-01.f

\ INTERFACE
: tempo 0 ad@ >> >> >> >> >> dyn-sync ;
: knob-select ad@ >> >> >> >> >> ;
variable length
: clip
    S1? if 16 length ! then
    S2? if  8 length ! then
    S3? if  4 length ! then
    ;
    
: rsndbd route bd1 ; bd2 ;
: rsndhh route hh ; ch ;
: rsndsd route sd1 ; sd2 ;
: rsndrd route rd1 ; rd2 ;

\ PATTERNS	
\ 4/4          *  .  .  .  *  .  .  .  *  .  .  .  *  .  .  .

: t0  pattern  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
: t1  pattern  .  .  .  .  .  .  .  .  .  .  .  .  .  .  o  .
: t2  pattern  .  .  o  .  .  .  .  .  .  o  .  .  .  .  .  .
: t3  pattern  .  o  .  .  .  .  .  o  .  o  .  .  .  .  .  .
: t4  pattern  o  .  .  .  o  .  .  .  o  .  .  o  .  .  .  .
: t5  pattern  .  .  .  o  .  o  o  .  .  .  o  .  o  .  .  o
: t6  pattern  o  .  .  .  o  .  .  .  o  .  o  .  o  .  o  .
: t7  pattern  o  .  o  .  o  .  o  o  o  .  o  .  o  o  o  o


: track-select
    route t0 ; t1 ; t2 ; t3 ; t4 ; t5 ; t6 ; t7 ;
    
\ MAIN LOOP
: pattern-sequencer
    0 time !
    note-sync
    for
	rbit rsndbd 1 knob-select track-select
	rbit rsndhh 2 knob-select track-select
	rbit rsndsd 3 knob-select track-select
	rbit rsndrd 4 knob-select track-select
	time 1+!
	tempo
	clip
    next
    silence ;


: metal
    16 length !
    begin length @ pattern-sequencer again ;

: main    restart metal ;
    