\ DK-01
\ basic drumkit

\ bass drum
\ square signal which frequency
\ quickly decrease
: bd1
    sound ->
    1 p0 square
    240 for p0@ 1 + p0 fast-sync next
    silence tail

\ snare drums
\ just some noise played for a little while    
: sd1
    sound ->
    noise
    2 for mod-sync next
    silence tail
    
\ ride
\ is a beep :)
: rd1
    sound ->
    0 p0 square
    2 for mod-sync next
    silence tail