\ solo.f
\ sheephero!

\ SYSTEM
load sys.f

\ SOUNDS
load SK-00.f

\ INTERFACE
: pitch
    4 ad@ \ pitch is controlled by knob 5
    >> >> >> >> >>
    oct !
    ;
\ knob 1 controls the notes length
: long-tone
    note0
    ld1 0 ad@ >> >> 1 + fast-notes ;
: short-tone
    note0
    ld1 0 ad@ >> >> >> >> 1 + fast-notes ;

\ hardcoded melody
: sequence
    pitch  0 long-tone
    pitch  0 long-tone 
    pitch  3 short-tone
    pitch  5 short-tone
    pitch  6 short-tone
    pitch 10 short-tone
    pitch  6 long-tone
    pitch 10 short-tone
    pitch  5 long-tone
    pitch 10 short-tone
    pitch  3 short-tone
    pitch 10 short-tone
    ;

\ LOOP!
\ possible bug here
    \ solo might crash (at startup)
    \ even though it is initialized properly    
: loooop begin sequence again ;
: solo restart loooop ;
    
