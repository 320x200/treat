\ sys.f
\ contains the different sync settings and some handy utils

\ SYNC

: fast-sync   5 sync ;   \ used for fast modulation
: mod-sync    9 sync ;   \ used for slow modulation
: note-sync  10 sync ;   \ 1/32 th notes
: fast-note-sync 8 sync ;
    
\ CONTROL

\ n -- \ retrigger the current sample for n notes
\ snd n -- \ play the sample snd for n notes    
: notes
    note-sync bang
     for note-sync next 
     silence ;

: fast-notes
    fast-note-sync bang
     for fast-note-sync next 
     silence ;
     
\ SYSTEM
     
: restart
    2 oct ! \ default octave
    engine-on init-board ;