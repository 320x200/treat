\ DK-01
\ drumkit

\ bass drums
: bd1
    sound ->
    1 p0 square
    15 for p0@ 2 + p0 fast-sync next
    60 p0
    90 for p0@ 1 + p0 fast-sync next
    silence tail

: bd2
    sound ->
    1 p0 square
    240 for p0@ 1 + p0 fast-sync next
    tail

\ snare drums    
: sd1
    sound ->
    1 p1 noise
    3 for p1@ 1 + p1 fast-sync next
    2 for mod-sync next
    silence tail

: sd2
    sound ->
    2 p1 noise
    2 for p1@ 1 + p1 mod-sync next
    1 for mod-sync next
    silence tail
    
\ ride
: rd1
    sound ->
    3 p0 4 p1 xmod2
    10 for p0@ 1 + p0 fast-sync next
    4 for mod-sync next
    20 for p1@ 4 + p1 fast-sync next
    silence tail

: rd2
    sound ->
    2 p0 3 p1 xmod2
    10 for p0@ 2 + p0 fast-sync next
    2 for mod-sync next
    5 for p1@ 2 + p1 fast-sync next
    silence tail

\ hihat
: ch
    sound ->
    noise    
    1 for mod-sync next
    5 for p1@ 2 + p1 fast-sync next
    silence tail

: oh
    sound ->
    noise 80 p1
    39 for
	p1@ 2 - p1 fast-sync
    next
    5 for mod-sync next
    tail

: hh
    sound ->
    noise
    5 for fast-sync next
    silence tail