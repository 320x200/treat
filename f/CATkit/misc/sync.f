\ sync.f
\ contains the different sync settings and some handy utils

\ SYNC

: fast-sync   5 sync ;   \ used for fast modulation
: mod-sync    9 sync ;   \ used for slow modulation
: note-sync  11 sync ;   \ 1/16 th notes

\ CONTROL

\ n -- \ retrigger the current sample for n notes
\ snd n -- \ play the sample snd for n notes    
: notes
    note-sync bang
     for note-sync next 
     silence ;

\ SYSTEM
     
: restart engine-on init-board ;