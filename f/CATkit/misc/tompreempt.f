: mod-sync 7 sync ;
: wobble   sound -> begin
                      square 50 p0 mod-sync
                      noise mod-sync
                    again
: note-sync 9 sync ;
: notes wobble for note-sync next silence ;