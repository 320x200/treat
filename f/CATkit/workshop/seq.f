\ sequencer

load sync.f  \ sync settings
load DK-01.f \ drumkit


\ the pattern language from synth.tex
     
variable  time

macro
: .       exit ;        \ don't do anything, just exit jump table.
: o       bang exit ;   \ run 'bang' then exit jump table.

: pattern
    time @    \ behaviour depends on global 'time' variable
    #x0F and  \ to simplify, all patterns have length 16
    route ;

forth

\ 4/4            *  .  .  .  *  .  .  .  *  .  .  .  *  .  .  .  

: trk1  pattern  o  .  .  .  o  .  .  .  o  .  .  o  .  .  .  .
: trk2  pattern  .  .  .  o  .  o  o  .  .  .  o  .  o  .  .  o
: trk3  pattern  .  .  o  .  .  .  .  .  .  .  .  .  .  .  .  .    
: trk4  pattern  .  .  .  .  .  .  .  .  .  .  .  .  .  o  .  .
: trk5  pattern  .  o  .  .  .  .  .  o  .  o  .  .  .  .  .  .
: trk6  pattern  .  .  .  .  .  .  .  .  .  .  .  .  .  .  o  .

    
\ n -- \ run the pattern sequencer for n notes
: pattern-sequencer
    0 time ! \ start with initialized time
    note-sync
    for
	\ select    \ pattern
	bd2    trk1
	bd1    trk2
	sd1    trk3
	sd2    trk4
	hh     trk5
	rd1    trk6
	
	time 1+!    \ advance time for the pattern words
	note-sync   \ our time base is notes
    next
    silence ;
