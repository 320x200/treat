\ sandbox.f
\ test some code here

\ SYSTEM ******************************************
load sys.f

\ SOUNDS ******************************************
\ NOTE: only load *ONE DRUMKIT AT A TIME* to avoid
\ name conflicts 

\ load DK-00.f \ a basic drumkit
load DK-01.f \ a more comple drumkit

load SK-00.f \ some synths presets

\ PLAYGROUND **************************************

\ you can test individuals sounds like this:
\ sound-name note-duration notes
\ ex: bd1 1 notes -> plays one kick

: pause
    sound ->
    silence
    6 for mod-sync next
    silence
    tail

: drum-sequence
    bd1 1 notes
    rd1 1 notes
    sd1 2 notes
    bd1 2 notes
    rd1 1 notes
    rd1 1 notes
    ;

\ bd1 bd2 sd1 sd2 rd1 rd2 ch oh hh
    
: superstuff
    for
	bd2 1 notes
	rd2 3 notes
	bd2 2 notes
	bd2 1 notes
        pause 5 notes
	ch  1 notes
	oh  3 notes
	bd1 1 notes
	bd1 1 notes
	sd2 2 notes
    next
    ;
    
\ try this:
\ 10 loop    
: loop for drum-sequence next ;

\ *************************************************

: square-sequence
     a sq1 1 notes
     g sq1 1 notes
     a sq1 1 notes
     f sq1 1 notes
     a sq1 1 notes
     e sq1 1 notes
     a sq1 1 notes
     d sq1 1 notes
    ;

\ try 3 tocata
: tocata
    for
	2 set-octave square-sequence
	3 set-octave square-sequence
	4 set-octave square-sequence
    next
    ;

\ ****************************************************

: mod1 p1 xq1 1 notes ;

: arp1
    25 a mod1
    25 a mod1
    25 a mod1
    12 a mod1
    ;

: arp2
    25 b mod1
    25 b mod1
    25 b mod1
    12 b mod1
    ;

\ try 10 arp    
: arp 3 set-octave for arp1 arp2 next ;

\ ******************************************************

: pulse pm1 1 notes ;
: width pw @ 3 + pw ! ;
: pulses-a for width a pulse next ;
: pulses-b for width d pulse next ;
    
: pulp
    for
	0 pw !
	4 pulses-a
	0 pw !
	4 pulses-b
    next
    ;