#!/bin/bash
# wikish 
#
# Copyright (C) 2008  Aymeric Mansoux
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# cgi response header
echo "Content-type: text/html"
echo ""

# query string
ID=`echo "$QUERY_STRING" | sed -n 's/^.*id=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`

# doctype
echo 	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \
	 \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
# head begins
echo	"<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">"
echo	"<head> \
	 <meta http-equiv=\"content-type\" content=\"text/html\; charset=utf-8\"/> \
	 <title>$ME</title>"
# style
echo	"<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>"

# head ends
echo	"</head>"

# body start
echo	"<body>"
echo	"<div class=\"container\">"
echo    "</div>"



# footer ------------------------------------------------------------------
echo    "<div class=\"footer\">"
#echo	"<pre>debug ID=$ID</pre>"
echo    "</div>"

# bye bye
echo	"</body></html>"
