#!/bin/bash
# Phoneutria Bahiensis spider
# wandering around and collecting stuff ...

# Config ---------------------------------------------------------------------
LAIR=~/bahiensis
SPOOF="Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"


# Functions ------------------------------------------------------------------

# Dictionnaries path validation
function validate_paths {

if test -e $LAIR ; then
        if [ $PF = 0 ] ; then
		echo "[!] Lair found!"
	fi
	else
        if [ $PF = 0 ] ; then
		echo "[!] Lair not found ... Nesting the spider ..."
        fi
	mkdir $LAIR $LAIR/tmp $LAIR/jpg $LAIR/png
	if test -e $LAIR ; then
	        if [ $PF = 0 ] ; then
			echo "[!] Lair created."
        	fi
		else
        	if [ $PF = 0 ] ; then
			echo "[!] can't create Lair ... bye."
		fi
		fi
	fi
}

# Collect the preys
function attack {

wget -l 0 -nv -r -H -A jpg,png -erobots=off --random-wait -P $LAIR/tmp \
 --inet4-only --cut-dirs=15 --tries=1 -T 5 --user-agent="$SPOOF" $OPTARG -o $STD ; 
 #| \
 #awk '/URL/ {print "[ ] attacking "$2}' 

}

# prepare the preys
function prepare { 

while true ; do
	for i in `find $LAIR/tmp -print` ; do
#        for i in `ls --sort=time $LAIR/tmp` ; do
		if echo "$i" | grep -q -E ".jpg|.JPG|.jpeg|.JPEG" ; then
			mv $i $LAIR/jpg/
			if [ $PF = 1 ] ; then
				FILE=`basename $i`
			        #echo $FILE
			        DOMAIN=`echo "$i" | sed s%$LAIR/tmp/%%g | sed s%/$FILE%%g`
			        #eval $DOMAIN
			        echo "\"$LAIR/jpg/$FILE\" jpg $DOMAIN"
			fi
		elif echo "$i" | grep -q -E ".png|.PNG" ; then
			mv $i $LAIR/png/
			if [ $PF = 1 ] ; then 
			        FILE=`basename $i`
				#echo $FILE
				DOMAIN=`echo "$i" | sed s%$LAIR/tmp/%%g | sed s%/$FILE%%g`
				#eval $DOMAIN
				echo "\"$LAIR/png/$FILE\" png $DOMAIN"
			fi
		fi
		sleep 0.2s
	done
done

}

# Options handling ------------------------------------------------------------
STD=/dev/stdout
PF=0
OPTSTRING="pht:"

# Check if any switches have been passed
if [ $# -eq 0 ] ; then
        echo "[!] Type -h for help."
        exit
fi

# Choose you destiny
while getopts "$OPTSTRING" SWITCH ; do
	case $SWITCH in
	h) echo "[?] Phoneutria Bahiensis spider"
	   echo "[?]"
	   echo "[?] Usage: bahiensis.sh [-h] -t location"
	   echo "[?] -h this help"
	   echo "[?] -p packet fourth mode"
	   echo "[?] -t hunt territory (http)"
	   echo "[?] Example: ./bahiensis.sh -t yahoo.com"
	   exit 0
	   ;;
	p) STD=/dev/null
	   PF=1
	   ;;
	t) validate_paths
	   ( prepare ) &
	   attack
           ;;
	*) echo "[!] Unexpected switch or argument"
	   echo "[!] Type -h for help."
	   exit 0
	   ;;
	esac
done			 
