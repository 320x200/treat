#!/bin/bash
#
# Etemenanki is an agent that builds word dictionnaries based on remote and
# local (hyper)text repositories.
# aym3ric-at-goto10-dot-org
# NO WARRANTY - USE IT AT YOUR OWN RISK


# Config ----------------------------------------------------------------------
GLOBAL_REPOS=~/dictionnaries/cache
DICOS=~/dictionnaries
PROXY=""
SPOOF="Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050511"
REJECT=ogg,iso,wav,mpg,mpeg,mp3,mov,jpg,jpeg,gif,png,bmp,tif,tga,xml,css,ico,js,gz,tgz,zip,rar,exe,bin,svg,pdf,eps,doc
WGET_RLEVEL=1
L33TIFY=0
OFFLINE=0

# Functions -------------------------------------------------------------------

# Dictionnaries path validation
function validate_paths {

if test -e $DICOS ; then
        echo "[!] dictionnaries folder found."
else
	echo "[!] dictionnaries folder not found ... creating ..."
	mkdir $DICOS
	mkdir $GLOBAL_REPOS
	if test -e $DICOS ; then
               	echo "[!] dictionnaries folder created."
        else
		echo "[!] can't create folder ... bye."
        fi
fi
										
}

# Grab the shit
function suck_and_run {

wget -l $WGET_RLEVEL -nv -nd -r -N -R $REJECT -P $LOCAL_REPOS \
 --user-agent="$SPOOF" -erobots=off --random-wait $OPTARG -o /dev/stdout | \
 awk '/URL/ {print "[ ] sucking "$2}' ;

}


# Garbage cleaner
function nogarbage {

sed 's/[.,;:)]*$//g' | \
 sed '/^http/ d' | sed '/^h77p/ d' |\
 sed '/^\// d' | \
 sed 's/^[[(]//g'

}

# Generate more words using l33t translations
function l33t {

if [ "$L33TIFY" = "1" ] ; then
        mv $DICOS/$OPTARG.wlist $DICOS/$OPTARG.wlist.tmp
	echo "[!] 4-PASS l33tifying in progress ..."
        echo "[ ] pass 1 in progress"
	cat $DICOS/$OPTARG.wlist.tmp | tr '[:upper:]' '[:lower:]' > $DICOS/$OPTARG.wlist.l33tlo
	cat $DICOS/$OPTARG.wlist.tmp | tr '[:lower:]' '[:upper:]' > $DICOS/$OPTARG.wlist.l33tup
	cat $DICOS/$OPTARG.wlist.l33tlo >> $DICOS/$OPTARG.wlist.tmp
	cat $DICOS/$OPTARG.wlist.l33tup >> $DICOS/$OPTARG.wlist.tmp
	rm $DICOS/$OPTARG.wlist.l33t*
	echo "[ ] pass 2 in progress"
	cat $DICOS/$OPTARG.wlist.tmp | tr 'aeiou' 'AEIOU' > $DICOS/$OPTARG.wlist.l33t
        cat $DICOS/$OPTARG.wlist.l33t >> $DICOS/$OPTARG.wlist.tmp
	rm $DICOS/$OPTARG.wlist.l33t
	echo "[ ] pass 3 in progress"
	cat $DICOS/$OPTARG.wlist.tmp | tr 'aeost' '43057' > $DICOS/$OPTARG.wlist.l33t
        cat $DICOS/$OPTARG.wlist.l33t >> $DICOS/$OPTARG.wlist.tmp
	rm $DICOS/$OPTARG.wlist.l33t
	echo "[ ] pass 4 in progress"
	cat $DICOS/$OPTARG.wlist.tmp | tr 'AEOST' '43057' > $DICOS/$OPTARG.wlist.l33t
	cat $DICOS/$OPTARG.wlist.l33t >> $DICOS/$OPTARG.wlist.tmp
	rm $DICOS/$OPTARG.wlist.l33t
	echo "[!] sorting $OPTARG.wlist - please wait"
	sort -u $DICOS/$OPTARG.wlist.tmp -o $DICOS/$OPTARG.wlist
	rm $DICOS/$OPTARG.wlist.tmp ;
fi

}

# Extract the words
function wlist_creator {

# pack/parse/clean all the gathered html crap

for i in `ls --sort=time --reverse $LOCAL_REPOS` ; do 
	echo "[ ] processing $i" ;
	WLIST_TMP="$WLIST_TMP `cat $LOCAL_REPOS$i | lynx -verbose off -dump -nolist -stdin | sed 's/*//g'`"
	if [ "${#WLIST_TMP}" -ge "20000" ] ; then
		echo $WLIST_TMP | sed 's/ /\n/g' | nogarbage > $DICOS/$OPTARG.unsortedchunk
		sort -u $DICOS/$OPTARG.unsortedchunk >> $DICOS/$OPTARG.wlist.tmp ;
		rm $DICOS/$OPTARG.unsortedchunk
		WLIST_TMP="";
	fi
done

# add the leftovers
echo $WLIST_TMP | sed 's/ /\n/g' | nogarbage > $DICOS/$OPTARG.unsortedchunk
sort -u $DICOS/$OPTARG.unsortedchunk >> $DICOS/$OPTARG.wlist.tmp ;
rm $DICOS/$OPTARG.unsortedchunk

# create or update the final dictionnary

if test -f $DICOS/$OPTARG.wlist ; then
	echo "[!] updating $OPTARG.wlist - please wait"
	cat $DICOS/$OPTARG.wlist.tmp >> $DICOS/$OPTARG.wlist
else
	echo "[!] creating $OPTARG.wlist - please wait"
	cat $DICOS/$OPTARG.wlist.tmp > $DICOS/$OPTARG.wlist
fi

echo "[!] sorting $OPTARG.wlist - please wait"
mv $DICOS/$OPTARG.wlist $DICOS/$OPTARG.wlist.tmp
sort -u $DICOS/$OPTARG.wlist.tmp -o $DICOS/$OPTARG.wlist ;
rm $DICOS/$OPTARG.wlist.tmp ;
l33t

}

# Merge all the wlist into a big dictionnary
function wlist_merge {

# test is there are any wlist around

if [ "`ls -1 $DICOS/*.wlist | wc -l`" -eq "0" ] ; then
	echo "[!] you don't have any wlist yet..."
else
	echo "[!] wlist found in $DICOS/"
	if [ "`ls -1 $DICOS/*.wlist | wc -l`" -eq "1" ] ; then
		echo "[!] you only have one though ... aborting."
	else
		for i in `ls -1 $DICOS/*.wlist | grep -v merge.wlist` ; do
			echo "[ ] merging $i"
			cat $i >> $DICOS/merge.wlist.tmp
		done
		echo "[ ] sorting the entries ..."
		sort -u $DICOS/merge.wlist.tmp -o $DICOS/merge.wlist
		rm $DICOS/merge.wlist.tmp
		echo "[!] merge completed - bye."
	fi
fi

}




# Options handling ------------------------------------------------------------
OPTSTRING="hd:lot:m"

# Check if any switches have been passed
if [ $# -eq 0 ] ; then
	echo "[!] Type -h for help."
	exit
fi

# Choose you destiny
while getopts "$OPTSTRING" SWITCH ; do
	case $SWITCH in
	h) echo "[?] Etemenanki - wordlist builder bot"
	   echo "[?] aym3ric-at-goto10-dot-org"
	   echo "[?]"
	   echo "[?] Usage: etemenanki.sh [-h] (-dlo) -t location"
	   echo "[?] -h this help"
	   echo "[?] -d maximum recursion depth (default=1)"
	   echo "[?] -l l33tify the words "
	   echo "[?] -o offline mode"
	   echo "[?] -t target location (http)"
	   echo "[?] -m merge all the wlist files"
	   echo "[?] Example: ./etemenanki -d 1 -l -t google.com"
	   exit 0
	   ;;
	d) WGET_RLEVEL=$OPTARG
	   ;;
	l) L33TIFY=1
	   ;;
	o) OFFLINE=1
	   ;;
	t) LOCAL_REPOS=$GLOBAL_REPOS/$OPTARG/
	   validate_paths
	   SUCKSTART=`date +%s`
	   if [ "$OFFLINE" = "0" ] ; then
	   	suck_and_run
	   fi
	   WLISTSTART=`date +%s`
	   wlist_creator
	   WLISTSTOP=`date +%s`
           echo "[!] $OPTARG.wlist written in $DICOS/"
	   echo "[!] sucked in `expr $WLISTSTART - $SUCKSTART` s - packed in `expr $WLISTSTOP - $WLISTSTART` s"
	   echo "[ ] bye..."
	   exit 0
	   ;;
	m) wlist_merge
	   exit 0
	   ;;
	*) echo "[!] Unexpected switch or argument"
	   echo "[!] Type -h for help."
	   exit 0
	   ;;
	esac
done
