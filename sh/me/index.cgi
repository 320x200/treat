#!/bin/sh
# me - the ideal website for me
# a Web0.1 minimal shell scripted flatfile cms tuned for me
# and developed as a tribute to me.
#
# Copyright (C) 2007  Aymeric Mansoux
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

VERSION="0.21"
ME="aymeric mansoux"
MAIL="aymeric -at- goto10 -dot- org"

# variables
PROJECTS=`ls projects/ | sed 's/\.[^.]*$//' | uniq`
UNKNOWN=1

# cgi response header
echo "Content-type: text/html"
echo ""

# query string
ID=`echo "$QUERY_STRING" | sed -n 's/^.*id=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`

# doctype
echo 	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \
	 \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
# head begins
echo	"<head> \
	 <meta http-equiv=\"content-type\" content=\"text/html\; charset=utf-8\"/> \
	 <title>$ME</title>"
# style
echo	"<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>"

# head ends
echo	"</head>"

# body start
echo	"<body>"
echo	"<div class=\"container\">"

# title

echo	"<h1>$ME/</h1>"

# nav begins --------------------------------------------------------------
echo	"<div class=\"nav\">"
# home
echo	"<pre><a href=\"/?id=home\">home</a></pre>"
echo    "<pre>-</pre>"
# log browser
echo    "<pre>log</pre>"
echo    "<pre>
<a href=\"/?id=P\">performances</a>
<a href=\"/?id=I\">installations</a>
<a href=\"/?id=L\">lectures/talks</a>
<a href=\"/?id=W\">workshops/courses</a>
<a href=\"/?id=A\">papers/articles</a>
</pre>"
echo    "<pre>-</pre>"
# project
echo    "<pre>projects</pre>"

echo	"<pre>"
for i in `echo $PROJECTS`; 
	do echo "<a href=\"/?id=$i\">$i</a>"; done
echo	"</pre>"
# contact
echo    "<pre>-</pre>"
echo    "<pre><a href=\"/?id=contact\">contact</a></pre>"

# nav ends
echo	"</div>"

# content -----------------------------------------------------------------
echo    "<div class=\"content\">"


if [ "$ID" = "" ] ; then
	ID="home"
fi

if [ "$ID" = "home" ] ; then
	echo    "<pre>last log entries:</pre>"
	echo    "<pre>"
	tail -n 8 log | sort -r -k 2,2 | sed 's/L/X/g' | sed 's/P/X/g' | sed 's/W/X/g'
	echo    "</pre>"
	UNKNOWN=0
elif [ "$ID" = "contact" ] ; then
	echo	"<p>$MAIL</p>"
	UNKNOWN=0
elif [ "$ID" = "P" -o "$ID" = "I" -o "$ID" = "L" -o "$ID" = "W" -o "$ID" = "A" ] ; then
	echo    "<pre>last log entries:</pre>"
        echo    "<pre>"
        cat log | sort -r -k 2,2 | grep $ID | sed 's/$ID/X/g'
	echo    "</pre>"
	UNKNOWN=0
else
	for i in `echo $PROJECTS`; do
		if [ "$ID" = "$i" ] ; then
			cat projects/$i.bla
			echo "<p>-</p>"
			for pic in `ls bits/pics/$i-*.png`; do
				echo "<div class="thumb"><a href="\#">"
				echo "<img src="$pic" width="80" height="64"></img>"
				echo "<img src="$pic" class="zoom"></img>"
				echo "</a></div>"
			done
		UNKNOWN=0
		fi
	done
fi

# 404
if [ "$UNKNOWN" = "1" ] ; then
	echo    "<h2>404</h2>"
fi

# content ends
echo    "</div>"



# footer ------------------------------------------------------------------
echo    "<div class=\"footer\">"
echo	"<pre><a href="https://devel.goto10.org/svn/unpacked/sh/me/">me</a> $VERSION</pre>"
#echo	"<pre>debug ID=$ID</pre>"
echo    "</div>"

# bye bye
echo	"</div></body></html>"
