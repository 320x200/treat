#!/bin/bash
# Sends 100 love letters to $1 ;)

for i in `seq 1 100`; do
	sendmail -t <<EOF
From: me@me.net
To: $1
Subject: Hey handsome

You're awesome.
:)

PS: oh yeah.
EOF
	sleep 1
done
