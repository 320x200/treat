#!/bin/bash
# A Network Theory Spam machine
# Usage: ./ntsm.sh good_old_stuff.pdf nettime-nl@nettime.org

GOODSTUFF=`pdftotext $1 - | dadadodo - -c 2 -w 72`

sendmail -t <<EOF
From: jean@poutre.fr
To: $2
Subject: on openness to experience

Dear list,

$GOODSTUFF

Bests,
Jean

EOF
