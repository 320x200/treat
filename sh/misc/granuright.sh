#!/bin/sh
#

SOURCE=/mnt/hd1/1/160/mp3/Spanish/bari
RENDER=~/granuright.wav
BLANK=~/blank.wav
GRAIN="0.04"

OFFSET="0"

cp $BLANK $RENDER

for track in `/usr/bin/ls $SOURCE/*.mp3`; do
	ecasound -b:1 \
	 -i $SOURCE/$track -y:31.0 -ea:100% \
	 -kos:1,0,100,25,1.5 -t:$GRAIN -o chunk.wav
	OFFSET=`echo "$GRAIN * 0.5 + $OFFSET" | bc -q`
	#ecasound -i chunk.wav -o $RENDER -y:$OFFSET
	ecasound -a:1 -i $RENDER -a:2 -i chunk.wav -y:$OFFSET -a:1,2 -o tmp.wav
	mv tmp.wav $RENDER
done

#rm chunk.wav
