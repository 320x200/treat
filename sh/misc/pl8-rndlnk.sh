#!/bin/bash
# dumb random link generator for placard8
# aym3ric@goto10.org
#

# CONFIGURATION
PLAYERPATH=/xtra/web/leplacard.org/www/placortado
RANDOMPATH=/xtra/web/leplacard.org/www/pl8yer/

#
NUMBER=$[ ( $$$(date +%s) * $RANDOM  ) + 2005 ]
rm -f $RANDOMPATH/*
ln -s $PLAYERPATH $RANDOMPATH/`echo $NUMBER | tr 0123456789 leplacard8`
