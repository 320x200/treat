#!/bin/sh
# simple watermarking script
# aym3ric -at- goto10 -dot- org

FINAL_SIZE="1024x768"
WATERMARK="/mnt/hd1/1/metabiosis/documentation/slide-logo.png"
SOURCE_FOLDER="/mnt/hd1/1/watersource"
TARGET_FOLDER="/mnt/hd1/1/metabiosis-slide"
BASENAME="slide"

# -----------------------------

COUNTER="0"

for i in `/usr/bin/ls $SOURCE_FOLDER`; do
	echo "processing $i > $BASENAME$COUNTER.png"
	convert -resize $FINAL_SIZE $SOURCE_FOLDER/$i $TARGET_FOLDER/tmp.png
	composite $WATERMARK $TARGET_FOLDER/tmp.png $TARGET_FOLDER/$BASENAME$COUNTER.png 
	COUNTER=`expr $COUNTER + 1`
done

rm $TARGET_FOLDER/tmp.png
