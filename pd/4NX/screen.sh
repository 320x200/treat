#!/bin/sh
# This needs to be added in xorg.conf, in "Screen"
#
#        SubSection "Display"
#                Virtual 2048 768
#        EndSubSection
#
xrandr --output LVDS1 --auto --output VGA1 -s 800x600 --right-of LVDS1
