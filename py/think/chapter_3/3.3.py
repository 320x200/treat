#!/usr/bin/python

def right_justify(s):
    space = ' '
    indent = 70 - len(s)
    print space*indent+s

right_justify('ni')
right_justify('nii')
right_justify('niii')
right_justify('niiii')
right_justify('niiiii')
