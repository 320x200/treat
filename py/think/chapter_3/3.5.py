#!/usr/bin/env python

# utils

def do_twice(f):
    f()
    f()

def do_four(f):
    do_twice(f)
    do_twice(f)

# elements

def plus():
    print '+',

def minus():
    print '-',

def pipe():
    print '|',

def blank():
    print ' ',

# 4 squares

def line(f1, f2, f3):
    f2()
    f1(f3)

def topbottom():
    line(do_four, plus, minus)

def leftright():
    line(do_four, pipe, blank)

def row(f):
    f(topbottom)
    plus()
    print
    f(leftright)
    pipe()
    print
    f(leftright)
    pipe()
    print
    f(leftright)
    pipe()
    print
    f(leftright)
    pipe()
    print

def row2():
    row(do_twice)

def row4():
    row(do_four)

def square(f1,f2):
    f1(f2)
    f1(topbottom)
    plus()

print '\n2 by 2'
square(do_twice, row2)
print '\n4 by 4'
square(do_four, row4)


