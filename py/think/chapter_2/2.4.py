#!/usr/bin/python

# 1. Sphere
r = 5
pi = 3.14159265
vol = (4.0/3.0)*pi*(r**3)

print "sphere"
print vol

# 2. Books
book = 24.95*0.4
order = 60
shipping = (order - 1)*0.75+3
wholesale = book*order+shipping

print "\nwholesale"
print wholesale

# 3. Breakfast
delimiter = ':'
easy = 8*60+15
tempo = 7*60+12
course = easy*2+tempo*3
departure = 6*60*60+52*60
breakfast = departure+course
hours = breakfast/60/60
minutes = int(breakfast/60.0/60.0%hours*60)

print "\nbreakfast"
print str(hours)+delimiter+str(minutes)
