#!/usr/bin/env python

def check_fermat(a, b, c, n):
    abn = a**n + b**n
    cn = c**n
    if n <= 2 :
        print "n is not big enough"
    elif abn == cn :
        print "Holy smokes, Fermat was wrong!"
    else :
        print "No, that doesnt work."

def prompt_fermat():
    a = int(float(raw_input('type a:\n')))
    b = int(float(raw_input('type b:\n')))
    c = int(float(raw_input('type c:\n')))
    n = int(float(raw_input('type n:\n')))
    check_fermat(a, b, c, n)

prompt_fermat()
