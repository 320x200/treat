#!/usr/bin/env python

# dummy f
def awesome():
    print 'cheezburger'

# using for loops
def for_do_n(function, n):
    for i in range(n):
        function()

# using nested
def recursion_do_n(function, n):
    if n <= 0:
        return    
    function()
    recursion_do_n(function, n-1)

# test
print "using for loops:"
for_do_n(awesome, 5)
print "using recursion:"
recursion_do_n(awesome, 5)
