#!/usr/bin/env python

from TurtleWorld import *
world = TurtleWorld()
bob = Turtle()

def koch(t, length):
    if length < 3:
        fd(t, length)
        return
    angle = 60
    koch(t, length/3)
    lt(t, angle)
    koch(t, length/3)
    rt(t, angle*2)
    koch(t, length/3)
    lt(t, angle)
    koch(t, length/3)

def snowflake(t, length):
    koch(t, length)
    rt(t, 120)
    koch(t, length)
    rt(t, 120)
    koch(t, length)

def uber_koch(t, length, angle1, angle2, threshold):
    if length < threshold:
        fd(t, length)
        return
    uber_koch(t, length/threshold, angle1, angle2, threshold)
    lt(t, angle1)
    uber_koch(t, length/threshold, angle1, angle2, threshold)
    rt(t, angle2)
    uber_koch(t, length/threshold, angle1, angle2, threshold)
    lt(t, angle1)
    uber_koch(t, length/threshold, angle1, angle2, threshold)

bob.delay = 0.0001

#koch(bob, 400)
#snowflake(bob, 200)
#uber_koch(bob, 400, 60, 120, 3)
uber_koch(bob, 100, 20, 45, 2)

wait_for_user()
