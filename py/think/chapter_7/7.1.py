#!/usr/bin/python

def print_n_rec(s, n):
    if n <= 0:
        return
    print s
    print_n_rec(s, n-1)

def print_n_ite(s, n):
    while n > 0:
        print s
        n = n - 1

print_n_rec("cat", 5)
print_n_ite("cheese", 5)
