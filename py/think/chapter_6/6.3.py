#!/usr/bin/env python

# Verbosy (but correct)
#def is_between(x, y, z):
#    if x <= y and y <= z :
#        return True
#    else :
#        return False

# Concisely
def is_between(x, y, z):
    return x <= y and y <= z


print is_between(1, 2, 3)
print is_between(3, 2, 1)
