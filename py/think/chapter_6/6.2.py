#!/usr/bin/env python

# STAGE 1
#def hypotenuse(a, b):
#    return 0.0

# STAGE 2
#def hypotenuse(a, b):
#    a_square = a**2
#    b_square = b**2
#    print 'a_square is', a_square
#    print 'b_square is', b_square
#    return 0.0

# STAGE 3
#def hypotenuse(a, b):
#    a_square = a**2
#    b_square = b**2
#    sum_square = a_square + b_square
#    print 'sum_square is', sum_square
#    return 0.0

# STAGE 4

import math

def hypotenuse(a, b):
    a_square = a**2
    b_square = b**2 
    sum_square = a_square + b_square
    result = math.sqrt(sum_square)
    return result

print hypotenuse(12, 5)

