#!/usr/bin/python

def first(word):
    return word[0]

def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]

def is_palindrome(string):
    string_size = len(string)
    if first(string) != last(string) :
        print 'no'
    elif middle(string) == '':
        print 'yes'
    else :
        string = middle(string)
        is_palindrome(string)

is_palindrome('yes') 	# no
is_palindrome('noon') 	# yes
is_palindrome('non')	# yes
