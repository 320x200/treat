#!/usr/bin/env python

def compare(x, y):
    if x > y :
        return 1
    elif x < y :
        return -1
    else :
        return 0

print compare(5, 6)
print compare(6, 5)
print compare(7, 7)
