#!/usr/bin/python

def chop(t):
    del t[0]
    del t[-1]
    return None

def middle(t):
    return t[1:-1]

