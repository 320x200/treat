#!/usr/bin/python

def cumulate(t1):
    t2 = []
    total = 0
    for n in t1:
        total += n
        t2.append(total)
    return t2

t1 = [ 1, 2, 3 ]
t2 = cumulate(t1)
print t2
