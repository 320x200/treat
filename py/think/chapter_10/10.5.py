#!/usr/bin/python
from random import *

classroom = []

def cumulate(t1):
    t2 = []
    total = 0
    for n in t1:
        total += n
        t2.append(total)
    return t2

def has_duplicate(t):
    for key in range(len(t)-1):
        if t[key] == t[key+1]:
            return True
            break
    return False

def populate(t):
    for i in range(23):
        t.append(randint(1, 365))

def birthday_clash(t):
    t = []
    populate(t)
    t.sort()
    return has_duplicate(t)

def clash_stat(t, n):
    stat = []
    for i in range(n):
        if birthday_clash(t):
            stat.append(1)
        else:
            stat.append(0)
    sum = ''
    total = len(stat)
    for i in range(total):
        sum = sum + stat[i]
    return sum / total

clash_stat(classroom, 10)
