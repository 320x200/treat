#!/usr/bin/env python

from TurtleWorld import *
import math

world = TurtleWorld()
bob = Turtle()


def polygon(t, length, n):
    for i in range (n):
        fd(t, length)
        rt(t, 360.0/n)

# First attempt
#def circle(t, r):
#    n = 64
#    circumference = math.pi * 2 * r
#    length = circumference / n
#    polygon(t, length, n)

# More elegant
def circle(t, r):
    circumference = math.pi * 2 * r
    n = int(circumference / 3) + 1
    length = circumference / n
    polygon(t, length, n)

bob.delay = 0.01
circle(bob, 100)

wait_for_user()
