#!/usr/bin/env python

from TurtleWorld import *

world = TurtleWorld()
bob = Turtle()
print bob

def square(t, length):
    for i in range (4):
        fd(t, length)
        rt(t)

def polygon(t, length, n):
    angle = 360./n
    for i in range (n):
        fd(t, length)
        rt(t, angle)

polygon(bob, 64, 7)

wait_for_user()
