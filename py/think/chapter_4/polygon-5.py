#!/usr/bin/env python

from TurtleWorld import *
import math

world = TurtleWorld()
bob = Turtle()


def polyline(t, n, length, angle):
    for i in range (n):
        fd(t, length)
        rt(t, angle)

def polygon(t, n, length):
    angle = 360.0 / n
    polyline(t, n, length, angle)

def arc(t, r, angle):
    arc_length = math.pi * 2 * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = float(angle) / n
    polyline(t, n, step_length, step_angle)

def circle(t, r):
    arc(t, r, 360)

bob.delay = 0.01
#arc(bob, 100, 32)
circle(bob, 100)

wait_for_user()
