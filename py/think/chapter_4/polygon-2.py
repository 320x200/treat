#!/usr/bin/env python

from TurtleWorld import *

world = TurtleWorld()
bob = Turtle()
print bob

def square(t, length):
    for i in range (4):
        fd(t, length)
        rt(t)

square(bob, 32)

wait_for_user()
