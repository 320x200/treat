#!/usr/bin/python

import pypm
import time

NUM_MSGS = 100 # number of MIDI messages for input before closing

INPUT=0
OUTPUT=1



def PrintDevices(InOrOut):
    for loop in range(pypm.CountDevices()):
        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        if ((InOrOut == INPUT) & (inp == 1) | (InOrOut == OUTPUT) & (outp ==1)):
            print loop, name," ",
            if (inp == 1):
                print "(input) ",
            else:
                print "(output) ",
            if (opened == 1):
                print "(opened)"
            else:
                print "(unopened)"
    print

def SlowSauce():
    PrintDevices(INPUT)
    devIn = int(raw_input("Type input number: "))
    MidiIn = pypm.Input(devIn)

    PrintDevices(OUTPUT)
    devOut = int(raw_input("Type ouput number: "))
    MidiOut = pypm.Output(devOut, 10)

    print "Midi Input opened. Reading ",NUM_MSGS," Midi messages..."
#    MidiIn.SetFilter(pypm.FILT_ACTIVE | pypm.FILT_CLOCK)
    while True:
        while not MidiIn.Poll(): pass
        MidiData = MidiIn.Read(1) # read only 1 message at a time
        print MidiData
        MidiOut.Write(MidiData)
        #print "time ",MidiData[0][1],", ",
        #print  MidiData[0][0][0]," ",MidiData[0][0][1]," ",MidiData[0][0][2], MidiData[0][0][3]
        # NOTE: most Midi messages are 1-3 bytes, but the 4 byte is returned for use with SysEx messages.
        time.sleep(0.01)
    del MidiIn

 
pypm.Initialize() # always call this first, or OS may crash when you try to open a stream

SlowSauce()

