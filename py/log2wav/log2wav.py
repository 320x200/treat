#!/usr/bin/python

import wave, array, random

class logtowav:
   def  __init__(self, log, bits, frequency):
       self.log = open(log)
       self.wav = wave.open('log.wav', 'w')
       self.wav.setparams((1, int(bits)/8, int(frequency), 0, 'NONE', 'noncompressed'))
       self.samplesize = pow(2, int(bits))
       self.peaks = []

   def normalize(self):
       for line in self.log:
           self.peaks.append(len(line.strip()))
       self.multiplier = self.samplesize / max(self.peaks)
       def scale(float):
           return self.multiplier * float
       return map(scale, self.peaks)

   def wavify(self):
       self.buffer = array.array('l', [0])
       self.buffer.fromlist(self.normalize())
       self.wav.writeframesraw(self.buffer)
       self.wav.close()


# small example
def main():
    if len(sys.argv) == 4:
        txt = logtowav(sys.argv[1], sys.argv[2], sys.argv[3])
        txt.wavify()
    else:
        print "Usage: /path/to/logfile bitrate(16, 8) samplerate(44100, 22050, ...)"

if __name__ == "__main__":
    import sys
    main()
