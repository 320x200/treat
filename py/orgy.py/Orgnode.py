"""
The Orgnode module consists of the Orgnode class for representing a
headline and associated text from an org-mode file, and routines for
constructing data structures of these classes.
"""

# Program written by Charles Cave   (charlesweb@optusnet.com.au)
# February - March 2009
# More information at
#    http://members.optusnet.com.au/~charles57/GTD
# Support for multiple tags added by Aymeric Mansoux <aymeric@goto10.org>
# June 2009


import re, sys
import datetime

def makelist(filename):
   """
   Read an org-mode file and return a list of Orgnode objects
   created from this file.
   """

   try:
      f = open(filename, 'r')
   except IOError:
      print "Orgnode. Unable to open file [%s] " % filename
      print "Program terminating."
      sys.exit(1)

   todos         = dict()  # populated from #+SEQ_TODO line
   level         = 0
   heading       = ""
   bodytext      = ""
   tags          = []
   sched_date    = ''
   deadline_date = ''
   nodelist      = []
   propdict      = dict()
   
   for line in f:
       hdng = re.search('^(\*+)\s(.*?)\s*$', line)
       if hdng:
          if heading: 
             thisNode = Orgnode(level, heading, bodytext, tags)
             if sched_date:
                thisNode.setScheduled(sched_date)
                sched_date = ""
             if deadline_date:
                thisNode.setDeadline(deadline_date)
                deadline_date = ''
             thisNode.setProperties(propdict)
             nodelist.append( thisNode )
             propdict = dict()
          level = hdng.group(1)
          heading =  hdng.group(2)
          bodytext = ""
          tags = []
          tagsrch = re.search('(.*?)\s*:(.*):',heading)
          if tagsrch:
              heading = tagsrch.group(1)
              tags = tagsrch.group(2).split(':')
       else:
           if line[:10] == '#+SEQ_TODO':
              kwlist = re.findall('([A-Z]+)\(', line)
              for kw in kwlist: todos[kw] = ""

           if line[:1] != '#':
               bodytext = bodytext + line

           if re.search(':PROPERTIES:', line): continue
           if re.search(':END:', line):        continue
           prop_srch = re.search('^\s*:(.*?):\s*(.*?)\s*$', line)
           if prop_srch:
              propdict[prop_srch.group(1)] = prop_srch.group(2)
              continue
           sd_re = re.search('SCHEDULED:\s+<([0-9]+)\-([0-9]+)\-([0-9]+)', line)
           if sd_re:
              sched_date = datetime.date(int(sd_re.group(1)),
                                         int(sd_re.group(2)),
                                         int(sd_re.group(3)) )
           dd_re = re.search('DEADLINE:\s*<(\d+)\-(\d+)\-(\d+)', line)
           if dd_re:
              deadline_date = datetime.date(int(dd_re.group(1)),
                                            int(dd_re.group(2)),
                                            int(dd_re.group(3)) )

   # write out last node              
   thisNode = Orgnode(level, heading, bodytext, tags)
   if sched_date:
      thisNode.setScheduled(sched_date)
   if deadline_date:
      thisNode.setDeadline(deadline_date)
   nodelist.append( thisNode )
              
   # using the list of TODO keywords found in the file
   # process the headings searching for TODO keywords
   for n in nodelist:
       h = n.Heading()
       todoSrch = re.search('([A-Z]+)\s(.*?)$', h)
       if todoSrch:
           if todos.has_key( todoSrch.group(1) ):
               n.setHeading( todoSrch.group(2) )
               n.setTodo ( todoSrch.group(1) )
       
   return nodelist


class Orgnode(object):
    """
    Orgnode class represents a headline, tags and text associated
    with the headline.
    """
    def __init__(self, level, headline, body, tag):
        """
        Create an Orgnode object given the parameters of level (as the
        raw asterisks), headline text (including the TODO tag), and
        first tag. The makelist routine postprocesses the list to
        identify TODO tags and updates headline and todo fields.
        """
        self.level = len(level)
        self.headline = headline
        self.body = body
        self.tag = tag
        self.todo = ""
        self.scheduled = ""       # Scheduled date
        self.deadline = ""        # Deadline date
        self.properties = dict()
        
    def Heading(self):
        """
        Return the Heading text of the node without the TODO tag
        """
        return self.headline

    def setHeading(self, newhdng):
        """
        Change the heading to the supplied string
        """
        self.headline = newhdng

    def Body(self):
        """
        Returns all lines of text of the body of this node except the
        Property Drawer
        """
        return self.body

    def Level(self):
        """
        Returns an integer corresponding to the level of the node.
        Top level (one asterisk) has a level of 1.
        """
        return self.level

    def Tag(self):
        """
        Returns a list of tags
        For example, :HOME:COMPUTER: would return ['HOME','COMPUTER']
        """
        return self.tag

    def setTag(self, newtag):
        """
        Change the value of the tag to the supplied string
        """
        self.tag = newtag

    def Todo(self):
        """
        Return the value of the TODO tag
        """
        return self.todo

    def setTodo(self, value):
        """
        Set the value of the TODO tag to the supplied string
        """
        self.todo = value

    def setProperties(self, dictval):
        """
        Sets all properties using the supplied dictionary of
        name/value pairs
        """
        self.properties = dictval

    def Property(self, keyval):
        """
        Returns the value of the requested property or null if the
        property does not exist.
        """
        return self.properties.get(keyval, "")
    
    def setScheduled(self, dateval):
        """
        Set the scheduled date using the supplied date object
        """
        self.scheduled = dateval

    def Scheduled(self):
        """
        Return the scheduled date object or null if nonexistent
        """
        return self.scheduled
    
    def setDeadline(self, dateval):
        """
        Set the deadline (due) date using the supplied date object
        """
        self.deadline = dateval

    def Deadline(self):
        """
        Return the deadline date object or null if nonexistent
        """
        return self.deadline

    def __repr__(self):
        """
        Print the level, heading text and tag of a node.
        """
        return "Level: %s Heading: %s Tag: [%s]" % (
            self.level, self.headline, self.tag)

    
