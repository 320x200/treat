#!/usr/bin/python

import Orgnode 
import cgi, re, os, fnmatch
import cgitb; cgitb.enable()
import datetime
import PyRSS2Gen

 
inputs = cgi.FieldStorage()
tag = inputs.getvalue("tag", "")
org = inputs.getvalue("org", "")
heading = inputs.getvalue("heading", "")
feed = inputs.getvalue("feed", "")


headernode = Orgnode.makelist("org/header.org") 

def cgipage():
    print "Content-Type: text/html"
    print

    print """
<!DOCTYPE HTML>
<html>
<head>
<title>test</title>
<link rel="stylesheet" type="text/css" href="orgy.css" />
</head>
<body>"""

    for header in headernode:
        print header.Body(),

def makenodes(path_to_org):
    result={}
    for file in os.listdir(path_to_org):
        # We want all the .org files but not the header 
        if fnmatch.fnmatch(file, '*.org') and file != 'header.org':
            result[file] = Orgnode.makelist('org/'+file)
    return result

orgnodes = makenodes('org')

def menu():
    print '<div class="menu">'
    for org in sorted(orgnodes.keys()):
        print '<span class="menuheaders">'+ org[:-len('.org')] +':</span><br>'
        if org != 'blog.org':
            for node in orgnodes[org]:
                if node.Heading() != inputs.getvalue("heading", ""):
                    print '<a class="menulinks" href="?org='+ org +'&heading='+ node.Heading() +'">'+ node.Heading() +'</a>'+' <br>'
                else:
                    print '<span class="menulinkscurrent">'+ node.Heading() +'</span>'+' <br>'
            #print '<br>'
        else:
            tags = []
            for node in orgnodes[org]:
                for tag in sorted(node.Tag()):
                    tags.append(tag)
            for tag in sorted(list(set(tags))):
                if tag != inputs.getvalue("tag", ""):
                    print '<a class="menulinks" href="?org='+ org +'&tag='+ tag +'">'+ tag +'</a>'+' <br>'
                else:
                    print '<span class="menulinkscurrent">'+ tag +'</span>'+' <br>'
            #print '<br>'
    print '</div>'


def htmlizer(string):
    string = re.sub('\[.*\]\n','', string)
    string = re.sub(r'file://(.*)png', r'<img src="\g<1>png"/>', string)
    string = string.replace('\n','<br>')
    return string

def date_extract(string):
    orgdate = re.search('\[.*\]', string).group(0)
    pubdate = orgdate[1:-1].split(' ').pop(0).split('-')
            
    orgdate = []
    for i in pubdate:
        orgdate.append(int(i))
    orgdate.extend([10, 10])
    return orgdate

def main():
    if feed == 'rss2':
        print "Content-Type: text/html"
        print

        items = []
        for node in orgnodes['blog.org']:
            items.append(PyRSS2Gen.RSSItem(
                    title = node.Heading(),
                    link = 'http://320x200.goto10.org/test/orgy?org=blog.org'+'&heading='+ node.Heading(),
                    description = htmlizer(node.Body()),
                    guid = PyRSS2Gen.Guid('http://320x200.goto10.org/test/orgy?org=blog.org'+'&heading='+ node.Heading()),
                    pubDate = datetime.datetime(*date_extract(node.Body()))))
        rss = PyRSS2Gen.RSS2(
                    title = "320x200",
                    link = "http://320x200.goto10.org",
                    description = "blablablablabla",
                    lastBuildDate = datetime.datetime.now(),
                    items = items)
        print rss.to_xml()
    else:
        cgipage()
        menu()
        print '<div class="content">'

        if org in orgnodes.keys() and org != 'blog.org':
            for node in orgnodes[org]:
                if node.Heading() == heading:
                    print '<div class="h1"><span class="h1">'+ heading +'</span></div>'
                    print '<div class="post">'+ htmlizer(node.Body()) +'</div>'
        elif tag:
            for node in orgnodes['blog.org']:
                if tag in node.Tag():
                    print '<div class="h1"><span class="h1">'+ node.Heading() +'</span></div>'
                    print '<div class="tags"><span class="tags">' + str(node.Tag()) + '</span></div>'
                    print '<div class="post">'+ htmlizer(node.Body()) +'</div>'
        elif org and heading:
            for node in orgnodes[org]:
                if heading == node.Heading():
                    print '<div class="h1"><span class="h1">'+ node.Heading() +'</span></div>'
                    print '<div class="tags"><span class="tags">' + str(node.Tag()) + '</span></div>'
                    print '<div class="post">'+ htmlizer(node.Body()) +'</div>'
        else:
            for node in orgnodes['blog.org']:
                print '<div class="h1"><span class="h1">'+node.Heading()+'</span></div>'
                print '<div class="tags"><span class="tags">' + str(node.Tag()) + '</span></div>'
                print '<div class="post">'+ htmlizer(node.Body()) +'</div>'
                            
        print '</content>'
        print '<div class="footer">powered by <a href="hgweb.cgi">very orgy</a> + <a href="http://goto10.org">GOTO10</a></div>'
        print '</body></html>'

main()
