function load()
   hikikomori = love.graphics.newImage("gphx/hiki.png")
   hikianim = love.graphics.newAnimation(hikikomori, 32, 32, 0.2)

   grass = love.graphics.newImage("gphx/grass.png")

   bob = love.graphics.newImage("gphx/bob.png")

   x,y = 320,240
   speed = 100
   w,h = 640,480
end

function draw_background()
   for j=0, 15 do
      for i=0, 20 do
         love.graphics.draw(grass, 32 * i, 32 * j)
      end
   end
end      

function update(dt)
   if math.floor(x) < 345 or math.floor(x) > 369 then
      if love.keyboard.isDown(love.key_right) and x < w - 16 then
	 x = x + (speed * dt)
	 hikianim:update(dt)
      elseif love.keyboard.isDown(love.key_left) and x > 0 + 16 then
	 x = x - (speed * dt)
	 hikianim:update(dt)
      end
   end

   if y < 266 or y > 288 then
      if love.keyboard.isDown(love.key_down) and y < h - 16 then
	 y = y + (speed * dt)
	 hikianim:update(dt)
      elseif love.keyboard.isDown(love.key_up) and y > 0 + 16 then
	 y = y - (speed * dt)
	 hikianim:update(dt)
      end
   end
end

function exit()
   if love.keyboard.isDown(love.key_escape) then
      love.system.exit()
   end 
end

function draw()
   print(x,y)
   exit()
   draw_background()
   love.graphics.draw(hikianim, math.floor(x), math.floor(y))
   love.graphics.draw(bob, 352, 272)
end
