#include <stdio.h>

/* print Celsius-Fahrenheit Table */
main ()
{
	float fahr, celsius, lower, upper;
	int step;

	lower = -17.8; 	/* lower limit of the temperature table */
	upper = 150.0;	/* upper limit */
	step = 10;	/* step size */

	/* print headers */
	printf("Celsius Fahrenheit\n");
        printf("------------------\n");

	celsius = lower;
	while (celsius <= upper) {
		fahr = (9.0/5.0) * celsius + 32.0 ;
		printf("%7.1f %9.0f\n", celsius, fahr);
		celsius = celsius + step;
	}
}
