#include <stdio.h>

/* transform input to output
   everything is 1, EOF is 0 */

main ()
{
	int c;

	/* exits on EOF */
	while (c) printf("%d", c = getchar() != EOF);

	/* never exits */
	/* while (c) printf("%d", getchar() != EOF); */

}
