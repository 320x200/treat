#include <stdio.h>

/* ASCII ramps */
/* cat some-file | a.out > /dev/dsp  */

main ()
{
	int c;
	int ramp;

	/* exits on EOF */
	while ((c = getchar()) != EOF) 
		for (ramp = 0; ramp <= c; ramp = ramp + 2)
		(putchar(c), putchar(ramp), putchar((c - ramp)));
}
